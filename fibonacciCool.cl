class Main inherits IO {
    main(): Object {
        let 
        	n : Int,
        	res : Int,
        	resmemo : Int
        	
        in {
           	out_string("Enter n: \n");
            n <-in_int();
            resmemo <- fibmemo(n,res);
            out_string("Res: ");
            out_int(resmemo);
            out_string("\n");
        }   
    };
    fib(n:Int):Int {
    	if 2 <= n then fib(n-1) + fib(n-2)
        else if n = 1 then 1
        else if n = 0 then 0
        else 0
        fi fi fi 
    };
    fibmemo(n:Int, res:Int):Int{
        if n= 1 then 1
        else if n <= res then res
        else fib(n-1) + fib(n-2)
        fi fi
    };
};